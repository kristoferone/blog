Title: First Post!
Date: 2016-03-25
Category: GitLab
Tags: pelican, gitlab
Slug: pelican-on-gitlab-pages

This is my first post

<p data-height="265" data-theme-id="0" data-slug-hash="pLaqbK" data-default-tab="result" data-user="microfront" data-embed-version="2" data-pen-title="Vue.js - Multi-select custom searchable dropdown with custom checkboxes" class="codepen">See the Pen <a href="https://codepen.io/microfront/pen/pLaqbK/">Vue.js - Multi-select custom searchable dropdown with custom checkboxes</a> by Dejan Babić (<a href="https://codepen.io/microfront">@microfront</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>